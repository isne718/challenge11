#include <iostream>
#include "Header.h"

using namespace std;

void main()
{
	List<int> list;
	int f; // input the number you want to find

	list.headPush(5);
	list.headPush(4);
	list.headPush(3);
	list.headPush(5);
	list.headPush(6);
	list.headPush(7);
	list.headPush(8);
	list.tailPush(1);
	list.tailPush(2);
	list.tailPush(3);
	list.tailPush(6);
	list.tailPush(6);
	list.tailPush(6);
	list.tailPush(6);
	list.display();
	cout << "\nHead Pop: ";
	cout << list.headPop();
	cout << "\nTail Pop: ";
	cout << list.tailPop();
	list.deleteNode(5);
	cout << "\nAfter delete node 5 list: ";
	list.display();
	cout << "\nEnter find number: ";
	cin >> f;
	if (list.isInList(f)) {
		cout << "Found";
	}
	else {
		cout << "Not Found";
	}

	list.sort();
	cout << "\nUse function to sort list: ";
	list.display();

	list.duplicate();
	cout << "\nAfter duplicate: ";
	list.display();


}