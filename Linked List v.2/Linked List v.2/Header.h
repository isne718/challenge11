#ifndef LIST2
#define LIST2

#include<iostream>
#include<vector>

using namespace std;

template<class T>
class Node {
public:
	T info;
	Node<T>* next, * prev;
	Node() { next = prev = 0; }
	Node(T el, Node<T>* n = 0, Node<T>* p = 0) { info = el; next = n; prev = p; }
};

template<class T>
class List {
public:
	List() { head = tail = 0; }
	~List();
	int isEmpty() { return head == 0; }
	void headPush(T); //Add element to front of list
	void tailPush(T); //Add element to tail of list
	T headPop(); //Remove and return element from front of list
	T tailPop(); //Remove and return element from tail of list
	void deleteNode(T); //Delete a particular value
	bool isInList(T); //Check if a particular value is in the list
	void display();//To display the function
	void sort();//sorting function
	void duplicate();//delete duplicate element function
private:
	Node<T> * head, * tail;
};

template<class T>
List<T>::~List() {
	for (Node<T>* p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

template<class T>
void List<T>::headPush(T num) { //head push function
	Node<T>* h = new Node<T>(num);
	if (head == 0) {
		head = h;
		tail = h;
	}
	else {
		h->next = head;
		head->prev = h; // go to another way
		head = h;
	}

}

template<class T>
void List<T>::display() { //display function
	Node<T>* dp = head;
	while (dp != 0) {
		cout << dp->info;
		dp = dp->next;
		cout << " ";
	}
}

template<class T>
void List<T>::tailPush(T num) { //tail push function
	Node<T>* t = new Node<T>(num);
	tail->next = t;
	t->prev = tail; //doubly 
	tail = t;
}

template<class T>
T List<T>::headPop() { //head pop function
	Node<T>* n = head;
	T store = head->info; //store value before delete
	head = head->next;
	delete n;
	return store;
}

template<class T>
T List<T>::tailPop() { //tail pop function
	Node<T>* p = tail;
	T store = tail->info; //store value before delete
	tail = tail->prev; //point to another way
	delete p;
	tail->next = 0;
	return store;
}

template<class T>
void List<T>::deleteNode(T num) { //delete function
	Node<T>* d = head, * d1, * d2;
	while (d->info != num) {
		d = d->next;//point to the previous node you want to delete
	}

	d1 = d->next; //point to the node you want to delete
	d2 = d->prev;//point to the next node you want to delete
	delete d;
	d2->next = d1;
	d1->prev = d2; //change the position of connected list
}

template<class T>
bool List<T>::isInList(T num) { //fund list function
	Node<T>* f = head;

	while (f != 0) {
		if (f->info == num) {
			return true;
		}
		else {
			f = f->next;
		}
	}

	return false;
}

template<class T>
void List<T>::sort() {
	vector<T>sort;
	Node<T>* it = head;
	while (it != NULL) { //push data to sort vector
		sort.push_back(it->info);
		it = it->next;
	}

	int swap; 
	T hold; //sorting by bubble sort
	do {
		swap = 0;
		for (int i = 0; i < sort.size() - 1; i++) {
			if (sort[i] > sort[i + 1]) {
				hold = sort[i];
				sort[i] = sort[i + 1];
				sort[i + 1] = hold;

				swap = 1;
			}
		}
	} while (swap == 1);

	//push sorted value to list
	it = head; //move it back to head
	int index = 0; //reset index to 0
	while (it != NULL) {
		it->info = sort[index];
		it = it->next;
		index++;
	}
}

template<class T>
void List<T>::duplicate() {
	Node<T>* start = head;

	while (start->next != NULL) {
		Node<T>* check = start->next;

		//loop to check duplicate value
		while (check->next != NULL) {
			if (check->info == start->info) { //if info of check is same info of start --> it's duplicate
				Node<T>* del = check; 
				Node<T>* l1 = check->prev;
				Node<T>* l2 = check->next;
				check = check->prev; // move check to previos position

				delete del;
				l1->next = l2;
				l2->prev = l1;
			}
			check = check->next; //move check to next position
		}

		start = start->next; //move start to next position
	}

}


#endif //LIST
