#include <iostream>

using namespace std;

template <class T> //change to use template
void swap_values(T& variable1, T& variable2){ //swap fuction
	T temp;
	temp = variable1;
	variable1 = variable2;
	variable2 = temp;
}

template <class T> 
int index_of_smallest(T a[], int start_index, int number_used){ //find the smallest value
	T min = a[start_index];
	int index_of_min = start_index;
	for (int index = start_index + 1; index < number_used; index++)
		if (a[index] < min){
			min = a[index];
			index_of_min = index;
		}
	return index_of_min;
}

template <class T> 
void sort(T a[], int number_used){ //sorting function
	int index_of_next_smallest;
	for (int index = 0;index < number_used - 1;index++){
		index_of_next_smallest = index_of_smallest(a, index, number_used);
		swap_values(a[index], a[index_of_next_smallest]);
	}
}

int main(){
	
	int a[5] = {4,3,5,2,1};
	
	cout<<"Before Sorting Integer: ";
	for(int i=0;i<5;i++){
		cout << a[i] <<" ";
	}
	cout<<endl;
	
	sort(a , 5); //called a sort function
	

	cout<<"Sorting Integer: ";
	for(int i=0;i<5;i++){
		cout << a[i] <<" ";
	}
	cout<<endl;
	
	
	
	
	char b[5] = {'c','d','b','a','e'};
	
	cout<<"Before Sorting Character: ";
	for(int i=0;i<5;i++){
		cout << b[i] <<" ";
	}
	cout<<endl;
	
	sort(b , 5);
	

	cout<<"Sorting Character: ";
	for(int i=0;i<5;i++){
		cout << b[i] <<" ";
	}
	cout<<endl;
	
	
	
	
	double c[5] = {1.25,5.25,4.25,3.25,2.25};
	
	cout<<"Before Sorting Decimal Number: ";
	for(int i=0;i<5;i++){
		cout << c[i] <<" ";
	}
	cout<<endl;
	
	sort(c , 5);
	

	cout<<"Sorting Decimal Number: ";
	for(int i=0;i<5;i++){
		cout << c[i] <<" ";
	}
	cout<<endl;
	
	
	
	
	
	return 0;
}
